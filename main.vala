
class FlowThing : Gtk.Widget {
  public int items {get; set; default = 0; }

  public FlowThing() {
    Object();

    notify["items"].connect(() => {queue_resize();});
  }

  // items are 100x100 8px border
  const double BORDER_WIDTH = 8.0;
  const double ITEM_SIZE = 100.0;
  const double TOTAL_ITEM_WIDTH = ITEM_SIZE + 2.0 * BORDER_WIDTH;
   
  private int columns = 0;
  private int rows = 0;

  public override void measure (Gtk.Orientation orientation, int for_size, out int minimum, out int natural, out int minimum_baseline, out int natural_baseline) {
    var w = get_width();
    var h = get_height();

    if (orientation == Gtk.Orientation.HORIZONTAL) {
        minimum = (int)TOTAL_ITEM_WIDTH;
        natural = (int)TOTAL_ITEM_WIDTH;
    } else {
      if (for_size == -1 && w == 0) {
          minimum = (int)TOTAL_ITEM_WIDTH;
          natural = (int)TOTAL_ITEM_WIDTH;
      } else {
        if (for_size == -1)  {
          for_size = w;
        }
        // calculate number of columns 
        columns = (int)Math.floor((double)for_size / (double)TOTAL_ITEM_WIDTH);

        // how many rows do we need for those columns
        rows = (items / columns) + 1;
        minimum = rows * (int)TOTAL_ITEM_WIDTH;
        natural = rows * (int)TOTAL_ITEM_WIDTH;
      }
    }

    minimum_baseline = -1;
    natural_baseline = -1;
    print("%s %d c: %d r: %d %d %d, wxh: %dx%d\n", orientation.to_string(), for_size, columns, rows, minimum, natural, w, h);
  }

  public override Gtk.SizeRequestMode get_request_mode() {
    return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH;    
  }

  public override void snapshot(Gtk.Snapshot snapshot) {
    var w = get_width();
    var h = get_height();

    snapshot.save();
    
    // calculate number of elements per row
    columns = (int)Math.floor((double)w / (double)TOTAL_ITEM_WIDTH);

    var r = Graphene.Rect.alloc();
    var c = Gdk.RGBA();
    var column = 0;
    var row = -1;    
    for (int i = 0; i < items; i++) {
      column = i % columns;
      if (column == 0) {
        row++;
      }
      c.red = i / 255.0f;
      c.green = 1.0f - (i / 255.0f);
      c.blue = 0.5f;
      c.alpha = 1.0f;
      snapshot.append_color(c, r.init((float)(column * TOTAL_ITEM_WIDTH + BORDER_WIDTH), (float)(row * TOTAL_ITEM_WIDTH), (float)ITEM_SIZE, (float)ITEM_SIZE));
    }
    snapshot.restore();
  }
}

int main(string[] args) {
    Gtk.init();
    var w = new Gtk.Window();
    var b = new Gtk.Box(Gtk.Orientation.VERTICAL, 16);
    var a = new FlowThing();
    var button = new Gtk.Button();
    button.set_label("Add Item");
    button.clicked.connect(() => {
      a.items++;
    });
    var enable = new Gtk.Switch();
    enable.hexpand = false;
    enable.vexpand = false;
    b.append(button);
    b.append(enable);
    var v = new Gtk.ScrolledWindow();
    v.set_child(a);
    v.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.ALWAYS);
    v.hexpand = true;
    v.vexpand = true;
    b.append(v);
    w.set_child(b);
    //w.set_size_request(800, 600);
    w.present();
    var loop = new GLib.MainLoop();
    w.close_request.connect(() => {
      loop.quit();

      return false;
    });
    loop.run();

    return 0;
}
